<?php

/**
 * @file
 * Include file to handle configuration screen.
 *
 * @ingroup nhd_seo
 */

/**
 * Form callback to set options for SEO configuration.
 *
 * @return array
 *   An HTML form.
 */
function nhd_seo_form($form, &$form_state) {
  $form = array();

  $form['nhd_seo']['nhd_enable_fragment_support'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable hashbang / _escaped_fragment_ support'),
    '#default_value' => variable_get('nhd_enable_fragment_support', TRUE),
    '#description' => t('<p>This option controls whether Drupal should respond to _escaped_fragment_ requests or not.</p>'),
  );

  $form['nhd_seo']['configuration_options_wrapper'] = array(
    '#type' => 'container',
    '#states' => array(
      // Hide the module's configuration options if fragment support is off.
      'invisible' => array(
        'input[name="nhd_enable_fragment_support"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form['nhd_seo']['configuration_options_wrapper']['nhd_enable_url_hook'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable url() hashbang rebuilding'),
    '#default_value' => variable_get('nhd_enable_url_hook', TRUE),
    '#description' => t("<p>Invokes hook_url_outbound_alter to modify the way the url() function builds URLs. It builds all internal URLs with #! (hashbang) syntax. This enables search engines to spider your site's internal links and discover content. Make sure your front end understands your Drupal URL alias structures.</p><p>NOTE: This will not affect hrefs inside &lt;a&gt; tags in content.</p>"),
  );

  $form['nhd_seo']['configuration_options_wrapper']['url_options_wrapper'] = array(
    '#type' => 'container',
    '#states' => array(
      // Hide the module's configuration options if fragment support is off.
      'invisible' => array(
        'input[name="nhd_enable_url_hook"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form['nhd_seo']['configuration_options_wrapper']['url_options_wrapper']['nhd_url_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('URL Prefix'),
    '#default_value' => variable_get('nhd_url_prefix', ''),
    '#description' => t("<p>Enter a prefix to add after the hashbang and before the fragment. Can be empty. Something as simple as a / could be useful depending on how your MVC routing wants to receive URLs. If you entered a / here: <strong>#!test-page-4</strong> would become <strong>#!/test-page-4</strong></p>"),
  );

  $form['nhd_seo']['configuration_options_wrapper']['url_options_wrapper']['nhd_enable_strip_base_url'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable $base_url stripping'),
    '#default_value' => variable_get('nhd_enable_strip_base_url', TRUE),
    '#description' => t('<p>Triggers stripping of the $base_url value inside hook_url_outbound_alter so the hashbang URL that is generated is free of the folder that Drupal lives in. This is useful when Drupal is installed in a subfolder of your javascript application. I.E. <strong>drupal/#!test-page-4</strong> becomes simply <strong>#!test-page-4</strong></p>'),
  );

  $themes = system_rebuild_theme_data();

  foreach ($themes as &$theme) {
    if (!empty($theme->info['hidden'])) {
      continue;
    }
    $theme_options[$theme->name] = $theme->info['name'];
  }

  $form['nhd_seo']['configuration_options_wrapper']['nhd_theme'] = array(
    '#type' => 'select',
    '#options' => array(0 => t('Default theme')) + $theme_options,
    '#title' => t('Custom Theme'),
    '#description' => t('<p>Choose a specific theme for use with _escaped_fragment_ requests. Very useful for optimizing the markup for speed.</p>'),
    '#default_value' => variable_get('nhd_theme', 0),
  );

  $form['nhd_seo']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Configuration'),
  );
  return $form;
}

/**
 * Process nhd_seo_form form submissions.
 */
function nhd_seo_form_submit($form, &$form_state) {
  drupal_set_message(t('The configuration options have been saved.'));

  // Save all the form values to variables.
  variable_set('nhd_theme', $form_state['values']['nhd_theme']);
  variable_set('nhd_enable_fragment_support', $form_state['values']['nhd_enable_fragment_support']);
  variable_set('nhd_enable_url_hook', $form_state['values']['nhd_enable_url_hook']);
  variable_set('nhd_url_prefix', $form_state['values']['nhd_url_prefix']);
  variable_set('nhd_enable_strip_base_url', $form_state['values']['nhd_enable_strip_base_url']);
}
