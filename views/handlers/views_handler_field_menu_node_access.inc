<?php

/**
 * @file
 * Handler for menu node access field.
 */

/**
 * Field handler to present whether the user has access to the menu link.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_menu_node_access extends views_handler_field {
  /**
   * Construct a new field handler.
   */
  public function construct() {
    parent::construct();
    $this->additional_fields['link_path'] = 'link_path';
  }

  /**
   * Called to add the field to a query.
   */
  public function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  /**
   * Render the field.
   *
   * @param object $values
   *   The values retrieved from the database.
   */
  public function render($values) {
    $link_path = $this->get_value($values, 'link_path');
    return drupal_valid_path($link_path);
  }
}
