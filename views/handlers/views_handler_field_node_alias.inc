<?php

/**
 * @file
 * Handler for alias field.
 */

/**
 * Field handler to present the alias for the node.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_node_alias extends views_handler_field {
  /**
   * Construct a new field handler.
   */
  public function construct() {
    parent::construct();
    $this->additional_fields['nid'] = 'nid';
  }

  /**
   * Called to add the field to a query.
   */
  public function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  /**
   * Render the field.
   *
   * @param object $values
   *   The values retrieved from the database.
   */
  public function render($values) {
    $nid = $this->get_value($values, 'nid');
    return drupal_get_path_alias("node/$nid");
  }
}
