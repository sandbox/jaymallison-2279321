<?php
/**
 * @file
 * Views style plugin to render nodes in the JSON data format.
 *
 * @see views_plugin_style_json.inc
 * @ingroup views_plugins
 */

/**
 * Implements hook_views_plugins().
 */
function nhd_json_views_plugins() {

  $path = drupal_get_path('module', 'nhd_json') . '/views';
  return array(
    'module' => 'nhd_json',
    'style' => array(
      'nhd_json' => array(
        'title'           => t('JSON'),
        'path'            => $path . '/plugins',
        'help'            => t('Displays nodes in the JSON data format.'),
        'handler'         => 'ViewsPluginStyleJson',
        'theme'           => 'views_nhd_json_style',
        'theme file'      => 'views_nhd_json_style.theme.inc',
        'theme path'      => $path . '/theme',
        'uses row plugin' => FALSE,
        'uses fields'     => TRUE,
        'uses options'    => TRUE,
        'type'            => 'normal',
        'help_topic'      => 'style-json',
        'even empty'      => TRUE,
      ),
    ),
  );
}

/**
 * Implements hook_views_data().
 */
function nhd_json_views_data() {

  $data['menu_links']['alias'] = array(
    'field' => array(
      'title' => t('Alias'),
      'help' => t('The resolved alias (if available) for this menu item.'),
      'handler' => 'views_handler_field_menu_node_alias',
    ),
  );

  $data['menu_links']['access'] = array(
    'field' => array(
      'title' => t('Access'),
      'help' => t('The return value of drupal_valid_path which checks for a valid path and if the user has access to it.'),
      'handler' => 'views_handler_field_menu_node_access',
    ),
  );

  $data['node']['alias'] = array(
    'field' => array(
      'title' => t('Alias'),
      'help' => t('The stored alias for this content. Differs from path by not appending the base URL'),
      'handler' => 'views_handler_field_node_alias',
    ),
  );

  return $data;
}
