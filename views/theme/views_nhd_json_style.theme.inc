<?php
/**
 * @file
 * Implements template preprocessors for json api
 */

/**
 * Views theme to render view fields as JSON.
 *
 * - $view: The view in use.
 * - $rows: Array of row objects as rendered by _nhd_json_render_fields
 * - $attachment: Not used currently
 * - $options: The options for the style passed in from the UI.
 *
 * @ingroup views_templates
 * @see nhd_json.views.inc
 */
function template_preprocess_views_nhd_json_style(&$vars) {
  $view = $vars["view"];
  $options = $vars["options"];
  $option_defs = $view->style_plugin->option_definition();
  $bitmasks = $option_defs['encoding']['contains'];

  // Create bitmask for json_encode.
  $bitmask = NULL;

  foreach ($bitmasks as $mask_key => $_bitmask) {
    if (isset($options[$mask_key]) && $options[$mask_key] && !is_array($options[$mask_key])) {
      $bitmask = $bitmask | constant($_bitmask['bitmask']);
    }
  }

  $vars['bitmask'] = $bitmask;

  // Adds pager support to JSON output.
  global $pager_total, $pager_page_array, $pager_total_items, $pager_limits;

  // Only add pager info if the pager is enabled.
  if ($pager_total[0] !== null && $pager_page_array[0] !== null) {
    // Assign pager data to rows output.
    $vars['rows']['pager'] = array(
      'pages' => $pager_total[0],
      'page' => $pager_page_array[0],
      'count' => intval($pager_total_items[0]),
      'limit' => intval($pager_limits[0])
    );
  }
}
